﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class AddedPropertyToPreferenceAndCustomers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_cutomers_promo_codes_promo_code_id",
                table: "cutomers");

            migrationBuilder.DropForeignKey(
                name: "fk_promo_codes_preferences_preference_id",
                table: "promo_codes");

            migrationBuilder.AlterColumn<Guid>(
                name: "preference_id",
                table: "promo_codes",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "promo_code_id",
                table: "cutomers",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "fk_cutomers_promo_codes_promo_code_id",
                table: "cutomers",
                column: "promo_code_id",
                principalTable: "promo_codes",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_promo_codes_preferences_preference_id",
                table: "promo_codes",
                column: "preference_id",
                principalTable: "preferences",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_cutomers_promo_codes_promo_code_id",
                table: "cutomers");

            migrationBuilder.DropForeignKey(
                name: "fk_promo_codes_preferences_preference_id",
                table: "promo_codes");

            migrationBuilder.AlterColumn<Guid>(
                name: "preference_id",
                table: "promo_codes",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "promo_code_id",
                table: "cutomers",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "fk_cutomers_promo_codes_promo_code_id",
                table: "cutomers",
                column: "promo_code_id",
                principalTable: "promo_codes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_promo_codes_preferences_preference_id",
                table: "promo_codes",
                column: "preference_id",
                principalTable: "preferences",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
