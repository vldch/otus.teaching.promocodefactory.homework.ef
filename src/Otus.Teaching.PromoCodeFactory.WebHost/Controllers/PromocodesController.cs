﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            return Ok(promocodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.Where(x => x.Name == request.Preference).FirstOrDefault();

            var customers = await _customerRepository.GetAllAsync();
            if (null == preference)
                return NoContent();
            var customersWithPreference = customers.Where(x => x.Preferences.Any(y => y.Preference.Id == preference.Id));
            if (null == customersWithPreference)
                return NoContent();

            var promocode = new PromoCode();
            promocode.PartnerName = request.PartnerName;
            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now.AddDays(10);
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
            promocode.Preference = preference;
            promocode.Customer = customersWithPreference.ToList();

            await _promocodeRepository.AddAsync(promocode);
            return CreatedAtAction(nameof(GivePromoCodesToCustomersWithPreferenceAsync), new { }, null);
        }
    }
}