﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Customer> Cutomers { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DataContext()
        {
        }
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(x => new { x.CustomerId, x.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(y => y.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(y => y.PreferenceId);
        }
    }
}
