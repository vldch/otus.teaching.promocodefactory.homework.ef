﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class InitializerDataBase : IInitializerDb
    {
        private readonly DataContext _context;
        public InitializerDataBase(DataContext dbContext)
        {
            _context = dbContext;
        }
        public void InitializerDb()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();

            _context.AddRange(FakeDataFactory.Employees);
            _context.SaveChanges();

            _context.AddRange(FakeDataFactory.Preferences);
            _context.SaveChanges();

            _context.AddRange(FakeDataFactory.Customers);
            _context.SaveChanges();

            _context.AddRange(FakeDataFactory.Roles);
            _context.SaveChanges();
        }
    }
}
