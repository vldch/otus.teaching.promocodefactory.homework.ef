﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength()]
        public string FirstName { get; set; }
        [MaxLength()]
        public string LastName { get; set; }
        [MaxLength()]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength()]
        public string Email { get; set; }

        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}