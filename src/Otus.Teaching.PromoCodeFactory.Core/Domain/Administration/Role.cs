﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength()]
        public string Name { get; set; }

        [MaxLength()]
        public string Description { get; set; }
    }
}